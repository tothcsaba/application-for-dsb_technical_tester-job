# Application for DSB_Technical_Tester job

## This project is an application for DSB's Technical Tester job  
Job link: https://candidate.hr-manager.net/ApplicationInit.aspx?cid=14&ProjectId=190213&MediaId=5  
See my CV and profile on LinkedIn: https://www.linkedin.com/in/csaba-toth-cph/  

## About me
#### My name is Csaba, I am a software test automation engineer with more than 5 years of experience.
#### In the followings, you will find my application material (that includes video demonstrations and detailed descriptions) I have prepared for this job call.

## 1. Mobile (Android) device tests
### I am experienced in mobile (Android) test automation. In this demo I worked with Jest, webdriver.io and Appium
#### Used technologies, platforms:
- Appium
- Webdriver.io
- NodeJS (ES6+, OOP)
  - Jest (test framework)
  - Crypto (Built-in Nodejs package for cryptography)
  - Other packages (see `package.json`)

#### Demos about Android automation:
- the `mobile.login.test.js`
  - Logs in (personal data encrypted)
  - Searches Journey based on given arrival and departure locations
  - Buys the next available ticket (with the default card which is stored in the test account within DSB's system)
    - only if you set the `pay` argument as `true` when you run the test.
- the `mobile.guest.test.js`
  - Remains guest,
  - Sets the date /time based on given date and time parameters
  - Checks the correct operation of the date/time picker

### Click on the image below to see a short demo video about Mobile automation (user)
[![Mobile: Tests as logged in user - Demo](http://img.youtube.com/vi/6EE54eXW3D4/0.jpg)](http://www.youtube.com/watch?v=6EE54eXW3D4 "Mobile: Tests as logged in user - Demo") 
### Click on the image below to see a short demo video about Mobile automation (guest)
[![Mobile: Tests as guest - Demo](http://img.youtube.com/vi/ci0k0SpM8bU/0.jpg)](http://www.youtube.com/watch?v=ci0k0SpM8bU "Mobile: Tests as guest - Demo")  
#### Try it yourself: 
- connect a testable device or start an emulator
- on a terminal window, run: `yarn appium` 
- another terminal window, 
  - run: `yarn test --testTimeout=77000 --from="Svanemøllen St." --to="Hellerup St." --pay=1 ./mobile.login.test.js` or
  - run: `yarn test --testTimeout=77000 --tripType="arrival" --date="2021-02-16" --time="09:00" ./mobile.guest.test.js`

## 2. Desktop browser tests with API tests integration
### Most of my experience is in browser testing with API integration. In this demo I worked with Jest and Puppeteer
#### Used technologies, platforms:
- NodeJS (ES6+, OOP)
  - Jest (test framework)
  - Puppeteer (framework for Chrome automation)
  - Crypto (Built-in Nodejs package for cryptography)
  - Other packages (see `package.json`)
- **Rejseplanen**'s Rest API (I have asked for permission)
- **Navitia**'s Rest API (I have asked for permission)

#### Demos about Browser testing + API integration:
- the `browser.test.js`
  - Searches Journey based on given arrival and departure locations and date/time parameters
  - On the result page 
    - Collects the results
    - Compares the results with `Navitia`'s API response
    - Compares the results with `Rejseplanen`'s API response
- the `browser.ddt.test.js`
  - Runs a complex Data Driven test based on the data of `testsAsTable.json`
  - Every time on result page:
    - Collects the results
    - Compares the results with `Navitia`'s API response
    - Compares the results with `Rejseplanen`'s API response
- the `browser.profile.test.js`
  - Logs in the DSB's profile page (https://www.dsb.dk/dsb-plus/minside/) (personal data encrypted)
  - Checks if there is a ticket with the given parameters. (In the video I have used the ticket which I have bought in Mobile automation demo)

### Click on the image below to see a short demo video about Browser + API integration (single test suite, emulated iPad pro within the browser)
[![Browser + API: Single test suite - Demo](http://img.youtube.com/vi/SeQscl_58E0/0.jpg)](http://www.youtube.com/watch?v=SeQscl_58E0 "Browser + API: Single test suite - Demo") 
### Click on the image below to see a short demo video about Browser + API integration (Data Driven style)
[![Browser + API: Data driven tests - Demo](http://img.youtube.com/vi/ZdOVhWPG4LE/0.jpg)](http://www.youtube.com/watch?v=ZdOVhWPG4LE "Browser: Data driven tests - Demo")
### Click on the image below to see a short demo video about Browser + Personal page check (with ticket checking)
[![Browser: Profile page test with ticket checking - Demo](http://img.youtube.com/vi/cHxPnH6LRik/0.jpg)](http://www.youtube.com/watch?v=cHxPnH6LRik "Browser: Profile page test with ticket checking - Demo")  
#### Try it yourself: 
- run: `yarn test --testTimeout=77000 --from="Hellerup St." --to="Helsingør St." --date="2021-02-16" --time="09:00" --device="iPad Pro" ./browser.test.js`
- run: `yarn test --testTimeout=77000 ./browser.ddt.test.js`
- run: `yarn test --testTimeout=77000 --id=55U89MJ9 --date=2021-01-15 --from="Svanemøllen St." --to="Hellerup St." --status="Betalt" ./browser.profile.test.js`

## 3. ReadyAPI + Groovy scripts
### I have never tried the ReadyAPI, nor have I ever written a script in Groovy, but I have significant experience in API testing.
#### Used technologies, platforms:
- ReadyAPI
- Groovy
- **Rejseplanen**'s Rest API (I have asked for permission)

#### Demo about ReadyAPI:
- Gets a `DataSource` from the Demo project's `testsAsTable.json` file
  - Converts the date format as requested 
- Gets the `From` location based on the converted `DataSource` (HTTP request)
- Gets the `To` location based on the converted `DataSource` (HTTP request)
- Gets `Journeys` based on
  - `From` location's `ID`, 
  - `To` location's `ID`,
  - converted `Date` parameter from `DataSource`, and
  - `Time` from `DataSource` (HTTP request)
- Saves responses into your local user's folder. (DataSink)
- Loops on `DataSource` until it is ended. (DataSource Loop)

### Click on the image below to see a short demo video about ReadyAPI + Groovy
[![ReadyAPI + Groovy: Data Driven tests with Rejseplanen’s API - Demo
](http://img.youtube.com/vi/tq-kFwVrpD8/0.jpg)](http://www.youtube.com/watch?v=tq-kFwVrpD8 "ReadyAPI + Groovy: Data Driven tests with Rejseplanen’s API - Demo
")  
#### Try it yourself: 
- import `REST Project 1-readyapi-project.xml` to your ReadyAPI. 
